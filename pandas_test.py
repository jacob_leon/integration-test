from adp.integration_test import integration_test
import pandas

###PANDAS###############################################################

data1 = [{"name": "jake", "dob": 'Dec 19'},
		 {"name": "lou", "dob": 'Feb 4'},
		 {"name": "cons", "dob": 'July 20'}]
		 
data2 = [{"name": "jake", "job": 'Consultant', "gender":'male'},
		 {"name": "cons", "job": "retired", "gender":"female"},
		 {"name": "lou", "job": "retired", "gender":'male'}]

df1 = pandas.DataFrame(data1)
df2 = pandas.DataFrame(data2)

m1 = df1.merge(df2,left_on='name',right_on='name',how='outer')

d1=m1.to_dict(orient='records')

print(d1)
###END PANDAS############################################################

##START DISTINCT#########################################################
jobs = data2[0]['job']

jobset = set()

[jobset.add(row['job']) for row in data2]

#print(jobset)
###END DISTINCT##########################################################


#print(data1[0])
#print(data2[1])

test_it = integration_test('test')

test_it.readJSONSourceFiles("dummy")
test_it.getMappingInfo("host=cadp-dev-dw.cnuaaqz8bupc.us-east-1.rds.amazonaws.com dbname=postgres user=centrouser password=centro123 dbname=dw_production",'MediaMind')



