import json, csv
from glob import glob

	
def flattenDict(d, result=None):
    if result is None:
        result = {}
    for key in d:
        value = d[key]
        if isinstance(value, dict): #if the value is a dictionary, parse the dict
            value1 = {}
            for keyIn in value:
                value1[".".join([key,keyIn])]=value[keyIn] #key formatting
            flattenDict(value1, result) #recursively call this code to get non-dict data
        elif isinstance(value, (list, tuple)):   
            for indexB, element in enumerate(value):
                if isinstance(element, dict):
                    value1 = {}
                    index = 0
                    for keyIn in element:
                        newkey = ".".join([key,keyIn])        
                        value1[".".join([key,keyIn])]=value[indexB][keyIn]
                        index += 1
                    for keyA in value1:
                        flattenDict(value1, result)   
        else:
            result[key]=value
    return result	
	
	
	
	
	
json_source='C:/Users/jacobl/Desktop/jsontocsv/json_in/'
csv_dest ='C:/Users/jacobl/Desktop/jsontocsv/csv_out/'

for infile in glob(json_source + '\*.json'):
	with open(infile, encoding='utf-8') as json_file:
		json_data = json.loads(json_file.read())
	print(type(json_data))
	flattened_json = flattenDict(json_data)
	print(type(flattened_json))
	f=open('C:/Users/jacobl/Desktop/flattened_json.txt','w',encoding='utf-8')
#	print([item for item in flattened_json], file=f)
	print(flattened_json, file=f)
	f.close()
	#print(infile.split("\\")[-1].split(".")[0] + .csv)
	raise SystemExit
	fo=open(csv_dest + ((infile.split("\\")[-1]).split(".")[0]) + ".csv",'w',encoding='utf-8')
	print(",".join(str("\"{0}\"").format(item) for item in flattened_json),file=fo)

	for row in flattened_json:
		print(",".join(str("\"{0}\"").format(item) for item in row.values()),file=fo)

	fo.close()
	
	
	