from adp.integration_test import integration_test
from datetime import datetime
from timeit import default_timer
start = default_timer()

moat_it = integration_test('moat')

missing_recs_filter = (lambda x,y: x['level1_label'] == y['Campaign_Name'] and x['impressions_analyzed'] == int(y['impression_analyzed']) and x['measurable_impressions'] == int(y['measurable_impressions']) and x['universal_interactions'] == float(y['engagement']))
duplicate_recs_filter = (lambda i,x,sd: sd[i]["level1_id"] == sd[x]["level1_id"] and sd[i]["level2_id"] == sd[x]["level2_id"] and sd[i]["level3_id"] == sd[x]["level3_id"] and sd[i]["level4_id"] == sd[x]["level4_id"])
mapping_recs_filter = (lambda j,r: r[0] == j['level3_id'] and datetime.strptime(j['date'],"%Y-%m-%d").date() >= r[1].date() and datetime.strptime(j['date'],"%Y-%m-%d").date() <= r[2].date())

#moat_it.downloadOutputFiles('.\csv')

moat_it.readJSONSourceFiles('./JSON/moat-acquired-data.json',"dummy")
moat_it.detectSourceDuplicates(duplicate_recs_filter)
moat_it.getMappingInfo("host=cadp-dev-dw.cnuaaqz8bupc.us-east-1.rds.amazonaws.com dbname=postgres user=centrouser password=centro123 dbname=dw_production",'MediaMind')
moat_it.findMappingDiscrepencies(mapping_recs_filter)
moat_it.readOutputCSVs('.\csv')
moat_it.findMissingRecs(missing_recs_filter)
moat_it.writeCSVReport('.')
#raise SystemExit


print(default_timer() - start)