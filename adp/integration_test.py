import json
import csv
import boto3
import io
import psycopg2, pandas
from datetime import datetime
from glob import glob
from pprint import pprint
from re import match,search

class integration_test:

	def __init__(self,partner):
		self.source = partner
		print("Constructor for " + self.source)
		
	def readOutputCSVs(self,source_path):
		self.output_data = self.__getCSVs(source_path)
	
	def readJSONSourceFiles(self,source_path,dest_path):
		with open(source_path, encoding='utf-8') as json_file:
			self.source_data = json.loads(json_file.read())['results']['details']
			
	def readCSVSourceFiles(self, source_path):
		self.source_data = self.__getCSVs(source_path)
					
	def detectSourceDuplicates(self, dupe_recs_filter):
		self.dupes_lst = self.__findDuplicatesInList(self.source_data, dupe_recs_filter)
		   
	def findMappingDiscrepencies(self, mapping_recs_filter):
		self.mp_match_lst, self.mp_non_match_lst = self.__compareTwoLists(self.source_data, self.map_recs, mapping_recs_filter)	
		
	def downloadOutputFiles(self,dest_path):
		__downloadFromS3("adp-client-reports",dest_path)
		
	def findMissingRecs(self,calc_diff):
		a,self.missing_rec_lst = self.__compareTwoLists(self.mp_match_lst, self.output_data, calc_diff)

	def writeCSVReport(self, report_path):
		if(len(self.missing_rec_lst) == 0):
			print("NO DATA TO WRITE")
			#raise NoDataToWriteError
		fo=open(report_path + '/{0}_discrepencies.csv'.format(self.source),'w',encoding='utf-8')
		print(",".join(str("\"{0}\"").format(item) for item in self.missing_rec_lst[0].keys()),file=fo)

		for row in self.missing_rec_lst:
			print(",".join(str("\"{0}\"").format(item) for item in row.values()),file=fo)

		fo.close()
		
	def getMappingInfo(self,connectionstr, source_filter):
		###Make connection to database#####################################
		datelist = tuple(self.__distinct(self.source_data,'date'))
		source_filter= tuple([source_filter])

		param_lst = source_filter + datelist
		conn=psycopg2.connect(connectionstr)
		ps_cur = conn.cursor()

		ps_SQL =   """    Select delivery_traffic_identifier,
								 flight_start,
								 flight_end
							from mapping.mapping_placement pl
					  INNER JOIN mapping.mapping_flight mf              
							  on pl.package_key = mf.package_key
					  INNER JOIN mapping.mapping_package mp
							  on mp.package_key = pl.package_key
						   where mp.delivery_traffic_system = %s """

					   
		for d in range(len(datelist)):

			if(d == 0 and len(datelist) ==1):
				ps_SQL = ps_SQL + """\n AND %s between mf.flight_start and mf.flight_end"""
			elif(d == 0 and len(datelist) >1):
				ps_SQL = ps_SQL + """\n AND (%s between mf.flight_start and mf.flight_end"""
			elif(d > 0 and d != len(datelist)-1): 
				ps_SQL= ps_SQL + """\n OR %s between mf.flight_start and mf.flight_end"""
			elif(d == len(datelist)-1):
				ps_SQL= ps_SQL + """\n OR %s between mf.flight_start and mf.flight_end)"""
		#print(ps_SQL)
		#print(param_lst)

		ps_cur.execute(ps_SQL, param_lst)
		self.map_recs =ps_cur.fetchall()	
		conn.close()
		
	
	def join(self, list_a, list_b,list_a_key,list_b_key, join_type):
		dfa = pandas.DataFrame(list_a)
		dfb = pandas.DataFrame(list_b)
		
		joined_rs = dfa.merge(dfb, left_on=list_a_key, right_on=list_b_key, how=join_type).to_dict(orient='records')
		return joined_rs	
		
	def __distinct(self,list_a, key):
		jobset = set()
		for row in list_a:
			jobset.add(row[key])
		return jobset
	
	def __downloadFromS3(bucket_name,dest_path):
		######DOWNLOAD S3 BUCKETS########################################
		csv_data = list()
		s3 = boto3.resource('s3')
		s3_file_path = dest_path
		for mybucket in s3.buckets.all():
			if(mybucket.name == bucket_name):
				maxdate = self.__findLatestSnapshotFile([str(object.key) for object in mybucket.objects.all() if match('.*\.csv',str(object.key))])
				for object in mybucket.objects.all():
					if(match('.*{0}\.csv'.format(maxdate),str(object.key))):
						filename =object.key.split('/')[-1]
						contents = object.get()
						with open(s3_file_path + filename,'wb') as file:
							txt = contents['Body'].read()
							file.write(txt)
							file.close
	
	def __findLatestSnapshotFile(self, filenames):
		#FIND THE LATEST TIMESTAMP
		maxdate = max([(search('\d{8}',f.split("_")[-1]).group(0)) for f in filenames])
		return maxdate	
		
	def __getCSVs(self,file_path):	
		csv_data = []
		maxdate = self.__findLatestSnapshotFile([file for file in glob(file_path + "/*.csv")])
		for infile in glob(file_path + '\*{0}.csv'.format(maxdate)):
			with open(infile,newline='') as csv_file:
				csv_read = csv.DictReader(csv_file,delimiter=',')
				for row in csv_read:
					csv_data.append(row)	
		return csv_data		
		
	def __findDuplicatesInList(self,in_list,dupe_recs_filter):	
		dupes_lst =[]	
		for i in range(len(in_list)):
			for x in range(i+1,len(in_list)):
				if(dupe_recs_filter(i,x,in_list)):
				   dupes_lst.append(in_list[i])
		return dupes_lst
		
	def __compareTwoLists(self,list_a, list_b, mapping_recs_filter):
		match_lst = []
		non_match_lst = []	

		for j in list_a:
			for r in list_b:
				if(mapping_recs_filter(j,r)):
					match_lst.append(j)
					break
			else:
				non_match_lst.append(j)
				
		return match_lst, non_match_lst