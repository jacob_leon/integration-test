import json
import csv
import boto3
import io
import psycopg2
from datetime import datetime
from glob import glob
from pprint import pprint
from re import match
from timeit import default_timer

start = default_timer()

download_resp = input('Re-download files from S3? ')

######DOWNLOAD S3 BUCKETS########################################
if(download_resp.lower() == 'y'):
	csv_data = list()
	s3 = boto3.resource('s3')
	s3_file_path = "C:/Users/jacobl/Desktop/Centro/csv/"
	for mybucket in s3.buckets.all():
	#	print(mybucket.name)
		if(mybucket.name == 'adp-client-reports'):
			for object in mybucket.objects.all():
	#			print(object.key)
				if(match('.*\.csv',str(object.key))):
					filename =object.key.split('/')[-1]
					contents = object.get()
	#				print(contents['Body'])
					with open(s3_file_path + filename,'wb') as file:
						txt = contents['Body'].read()
						file.write(txt)
						file.close
	#					print(txt)


###Make connection to database#####################################
conn=psycopg2.connect("host=cadp-dev-dw.cnuaaqz8bupc.us-east-1.rds.amazonaws.com dbname=postgres user=xxx password=xxx dbname=xxx")
ps_cur = conn.cursor()

ps_SQL =   """Select traffic_system_identifier,
				   flight_start,
				   flight_end
			  from mapping.mapping_placement mp
		INNER JOIN mapping.mapping_flight mf	
				on mp.package_key = mf.package_key
			 where mp.traffic_system = 'MediaMind'"""

ps_cur.execute(ps_SQL)
map_recs =ps_cur.fetchall()

#for r in map_recs:
#	print(r[1])

	
#####READ IN JSON FILE##############################################
with open('./JSON/moat-acquired-data.json', encoding='utf-8') as json_file:
	json_data = json.loads(json_file.read())['results']['details']


#####DUPE CHECK####################################################################
dupe_recs = 0
total_recs =0

for i in range(len(json_data)):
#	print(json_data[i]["level1_id"])
	total_recs =total_recs+1
	for x in range(i+1,len(json_data)):
		if(json_data[i]["level1_id"] == json_data[x]["level1_id"] and
		   json_data[i]["level2_id"] == json_data[x]["level2_id"] and 
		   json_data[i]["level3_id"] == json_data[x]["level3_id"] and 
		   json_data[i]["level4_id"] == json_data[x]["level4_id"]
		   ):
			#print("CampaignMatch!!!")
			dupe_recs=dupe_recs+1

print("Total Recs: " + str(total_recs) + "\nDupe Recs: " + str(dupe_recs))			
			
	

####FILTER OUT JSON RECORDS THAT DONT MATCH MAPPING TABLE ##################
match_lst = []
non_match_lst = []	
for j in json_data:
	for r in map_recs:
		if(r[0] == j['level3_id'] and 
					datetime.strptime(j['date'],"%Y-%m-%d").date() >= r[1].date() and datetime.strptime(j['date'],"%Y-%m-%d").date() <= r[2].date()):
			#print('Success')
			match_lst.append(j)
			break
	else:
		non_match_lst.append(j)
		#print('Non Matching Record Found')
		#print(type(json_data['results']['details']))
		#print(type(j))
		#print(type(map_recs))
		#print(type(r))

	
#for re in match_lst:
#		print(re)
	
	#pprint(json_data[';results']['details'][0]) #need to iterate over this level (collection of dicts?)
#print(json_data)
rowcount = 0


#####READ IN ALL CSV FILES###########################################
csv_data = []
for infile in glob('.\csv\*.csv'):
#	print(infile)
	with open(infile,newline='') as csv_file:
		csv_read = csv.DictReader(csv_file,delimiter=',')
#		csv_data = csv_data + csv_read
		for row in csv_read:
			csv_data.append(row)
		#csv_data = list(csv_read)
		#csv_data.append(csv_read)
		

	
#for i in csv_data:
#	print(i['Campaign_Name'])
	

#for j_row in json_data['results']['details']:
#	if j_row['level1_label'] == 'MDANDERSON005':
#		print('Success')


####COMPARE JSON TO CSV FILES##############################################	
json_cnt = 0
	
fo=open('./moat_discrepencies.csv','w')
fo.write("Campaign,Impression_Analyzed,Measurable_Impressions,In_View_Impressions,Engagement,Delivery_Date \n")
for j_row in match_lst: #json_data['results']['details']: 
	#print(j_row['level1_label'] + 'outer loop')
	json_cnt = json_cnt + 1
	for c_row in csv_data:
#		print(j_row['level1_label']+ '-' + c_row['Campaign_Name'])	
#		print(j_row['impressions_analyzed'] + '~' + c_row['impression_analyzed'])
#		print(j_row['level1_label']+ ' '+c_row['Campaign_Name']+ '\t' + str(j_row['impressions_analyzed']) + ' ' + c_row['impression_analyzed'] + '\t' + j_row['measurable_impressions'] + ' ' +c_row['measurable_impressions'])			
		if(
			j_row['level1_label'] == c_row['Campaign_Name'] and
			j_row['impressions_analyzed'] == int(c_row['impression_analyzed']) and
			j_row['measurable_impressions'] == int(c_row['measurable_impressions'])
			):
#			print("------------------------------Success-------------------------------------------")
#			print(j_row['level1_label']+ ' '+c_row['Campaign_Name']+ '\t' + str(j_row['impressions_analyzed']) + ' ' + c_row['impression_analyzed'] + '\t' + j_row['measurable_impressions'] + ' ' +c_row['measurable_impressions'])			
			break
	else:
		fo.write(j_row['level1_label']+ ','+ str(j_row['impressions_analyzed']) +','+str(j_row['measurable_impressions'])
					+','+str(j_row['in_view_impressions'])+','+str(j_row['universal_interactions'])+','+j_row['date']+"\n")
		
		

fo.close()
print(json_cnt)		

print(default_timer() - start)